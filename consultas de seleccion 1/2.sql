﻿USE ciclistas;

-- (01) Número de ciclistas que hay

SELECT COUNT(dorsal) FROM ciclista;

-- (02) Número de ciclistas que hay del equipo Banesto

SELECT COUNT(*) FROM ciclista 
  WHERE nomequipo = 'Banesto';

-- (03) Edad media de los ciclistas

SELECT AVG(edad) FROM ciclista;

-- (04) La edad media de los del equipo Banesto

SELECT AVG(edad) FROM ciclista 
  WHERE nomequipo = 'banesto';

-- (05) La edad media de los ciclistas por cada equipo

SELECT nomequipo, AVG(edad) FROM ciclista 
  GROUP BY nomequipo;

-- (06) El número de ciclistas por equipo

SELECT nomequipo, COUNT(*) FROM ciclista 
  GROUP BY nomequipo;

-- (07) El número total de puertos

SELECT COUNT(*) FROM puerto;

-- (08) El número total de puertos mayores de 1500

SELECT COUNT(*) FROM puerto
  WHERE altura>1500;

-- (09) Listar el nombre de los equipos que tengan más de 4 ciclistas

SELECT nomequipo FROM ciclista 
  GROUP BY nomequipo HAVING COUNT(*)>4;

-- (10) Listar el nombre de los equipos que tengan más de 4 ciclistas cuya edad esté entre 28 y 32

SELECT nomequipo FROM ciclista
  WHERE edad BETWEEN 28 AND 32
  GROUP BY nomequipo HAVING COUNT(*)>4;

-- (11) Indícame el número de etapas que ha ganado cada uno de los ciclistas

SELECT dorsal, COUNT(*) FROM etapa 
  GROUP BY dorsal;

-- (12) Indícame el dorsal de los ciclistas que hayan ganado más de una etapa

SELECT dorsal FROM etapa 
  GROUP BY dorsal HAVING COUNT(*)>1