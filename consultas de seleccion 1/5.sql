﻿-- (1) Nombre y edad de los ciclistas que NO han ganado etapas

SELECT DISTINCT nombre, edad FROM ciclista LEFT JOIN etapa ON ciclista.dorsal = etapa.dorsal WHERE ciclista.dorsal IS NULL;

-- (2) Nombre y edad de los ciclistas que NO han ganado puertos

SELECT DISTINCT nombre, edad FROM  ciclista LEFT JOIN puerto ON ciclista.dorsal = puerto.dorsal WHERE ciclista.dorsal IS NULL;

-- (3) Listar el director de los equipos que tengan ciclistas que NO hayan ganado ninguna etapa

SELECT DISTINCT director FROM equipo LEFT JOIN ciclista LEFT JOIN etapa ON ciclista.dorsal = etapa.dorsal ON equipo.nomequipo = ciclista.nomequipo WHERE etapa.dorsal IS NULL;

SELECT director FROM equipo;

-- (4) Dorsal y nombre de los ciclistas que NO hayan llevado ningún maillot

SELECT DISTINCT ciclista.dorsal, nombre FROM ciclista LEFT JOIN lleva ON ciclista.dorsal = lleva.dorsal WHERE lleva.dorsal IS NULL;

-- (5) Dorsal y nombre de los ciclistas que NO hayan llevado el maillot amarillo NUNCA

SELECT DISTINCT dorsal FROM lleva JOIN maillot ON lleva.código = maillot.código WHERE color='amarillo';

SELECT ciclista.dorsal, nombre FROM ciclista LEFT JOIN (SELECT  DISTINCT dorsal FROM lleva JOIN maillot ON lleva.código = maillot.código WHERE color='amarillo')c1 ON ciclista.dorsal = c1.dorsal WHERE c1.dorsal IS NULL;

SELECT DISTINCT ciclista.dorsal FROM ciclista JOIN lleva ON ciclista.dorsal = lleva.dorsal JOIN maillot ON lleva.código = maillot.código
  WHERE color='amarillo';

SELECT COUNT(dorsal) FROM ciclista;