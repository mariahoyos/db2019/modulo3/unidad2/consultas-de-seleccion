﻿-- (1) Nombre y edad de los ciclistas que han ganado etapas

SELECT DISTINCT nombre, edad FROM ciclista JOIN etapa USING (dorsal);

-- (2) Nombre y edad de los ciclistas que han ganado puertos

SELECT DISTINCT nombre, edad FROM ciclista JOIN puerto USING (dorsal);

-- (3) Nombre y edad de los ciclistas que han ganado etapas y puertos

SELECT DISTINCT nombre, edad FROM ciclista JOIN etapa USING(dorsal) JOIN puerto USING(dorsal);

-- (4) Listar el director de los equipos que tengan ciclistas que hayan ganado alguna etapa

SELECT DISTINCT director FROM equipo JOIN ciclista USING(nomequipo) JOIN etapa USING(dorsal);

-- (5) Dorsal y nombre de los ciclistas que hayan llevado algún maillot

SELECT DISTINCT dorsal, nombre FROM ciclista JOIN lleva USING(dorsal);

-- (6) Dorsal y nombre de los ciclistas que hayan llevado el maillot amarillo

SELECT DISTINCT dorsal,nombre FROM ciclista JOIN lleva USING(dorsal) JOIN maillot USING(código) WHERE color='amarillo';

-- (7) Dorsal de los ciclistas que hayan llevado algún maillot y que hayan ganado etapas

SELECT DISTINCT dorsal FROM etapa JOIN lleva USING(dorsal);

-- (8) Indicar el numetapa de las etapas que tengan puertos

SELECT DISTINCT etapa.numetapa FROM etapa JOIN puerto USING(numetapa);

-- (9) Indicar los km de las etapas que hayan ganado ciclistas del Banesto y que tengan puertos

SELECT DISTINCT kms, etapa.numetapa FROM ciclista JOIN etapa USING(dorsal) JOIN puerto USING(numetapa) WHERE nomequipo='Banesto';


SELECT DISTINCT kms,etapa.numetapa FROM ciclista JOIN etapa ON ciclista.dorsal = etapa.dorsal JOIN puerto ON etapa.numetapa = puerto.numetapa WHERE nomequipo="banesto";

SELECT * FROM puerto JOIN ciclista ON puerto.dorsal = ciclista.dorsal;

SELECT DISTINCT `etapa`.`kms`, `puerto`.`numetapa` 
  FROM `puerto` inner join `ciclista` 
  ON `puerto`.`dorsal` = `ciclista`.`dorsal` 
  INNER JOIN `etapa` 
  ON etapa.numetapa=puerto.numetapa WHERE ciclista.nomequipo='Banesto';

-- (10) Listar el número de ciclistas que hayan ganado alguna etapa con puerto

SELECT COUNT(DISTINCT etapa.dorsal)AS cuenta FROM etapa JOIN puerto ON etapa.numetapa = puerto.numetapa;

-- (11) Indicar el nombre de los puertos que hayan sido ganados por ciclistas de Banesto

SELECT DISTINCT nombre FROM ciclista JOIN puerto ON ciclista.dorsal = puerto.dorsal WHERE nomequipo='Banesto';

-- (12) Listar el número de las etapas que tengan puerto que hayan sido ganadas por ciclistas de Banesto con más de 200km o 200km

SELECT DISTINCT etapa.numetapa FROM etapa JOIN ciclista ON etapa.dorsal = ciclista.dorsal JOIN puerto ON etapa.numetapa = puerto.numetapa WHERE nomequipo='Banesto' AND kms>=200;