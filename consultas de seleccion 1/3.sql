﻿USE ciclistas;

-- (01) Listar las edades de todos los ciclistas de Banesto

SELECT DISTINCT edad FROM ciclista
  WHERE nomequipo='Banesto';

-- (02) Listar las edades de los ciclistas que son de Banesto o de Navigare

SELECT DISTINCT edad FROM ciclista
  WHERE nomequipo='Banesto' OR nomequipo='Navigare';

-- (03) Listar el dorsal de los ciclistas que son de Banesto y cuya edad está entre 25 y 32

SELECT dorsal FROM ciclista
  WHERE nomequipo='Banesto' OR edad BETWEEN 25 AND 32;

-- (04) Listar el dorsal de los ciclistas que son de Banesto o cuya edad está entre 25 y 32

SELECT dorsal FROM ciclista
  WHERE nomequipo='Banesto' AND edad BETWEEN 25 AND 32;

-- (05) Listar la inicial del equipo de los ciclistas cuyo nombre comience por R

SELECT LEFT(nomequipo,1) FROM ciclista
  WHERE LEFT(nombre,1)='R';

-- (06) Listar el código de las etapas que su salida y llegada sea en la misma población

SELECT numetapa FROM etapa
  WHERE salida=llegada;

-- (07) Listar el código de las etapas que su salida y llegada no sean en la misma población y que conozcamos el dorsal del ciclista que ha ganado

SELECT numetapa FROM etapa
  WHERE salida<>llegada AND dorsal IS NOT NULL;

-- (08) Listar el nombre de los puertos cuya altura esté entre 1000 y 2000 o que la altura sea mayor que 2400

SELECT nompuerto FROM puerto
  WHERE altura BETWEEN 1000 AND 2000 OR altura>2400;

-- (09) Listar el dorsal de los ciclistas que hayan ganado los puertos cuya altura esté entre 1000 y 2000 o que la altura sea mayor que 2400

SELECT DISTINCT dorsal FROM puerto
  WHERE altura BETWEEN 1000 AND 2000 OR altura>2400;

-- (10) Listar el número de ciclistas que hayan ganado alguna etapa

SELECT COUNT(DISTINCT dorsal) FROM etapa;

-- (11) Listar el número de etapas que tengan puerto

SELECT COUNT(DISTINCT numetapa) FROM puerto;

-- (12) Listar el número de ciclistas que hayan ganado algún puerto

SELECT COUNT(DISTINCT dorsal) FROM puerto;

-- (13) Listar el código de la etapa con el número de puertos que tiene

SELECT numetapa, COUNT(*) FROM puerto
  GROUP BY numetapa;

-- (14) Indicar la altura media de los puertos

SELECT AVG(altura) FROM puerto;

-- (15) Indicar el código de etapa cuya altura media de sus puertos está por encima de 1500

SELECT numetapa FROM puerto
  GROUP BY numetapa HAVING AVG(altura)>1500;

-- (16) Indicar el número de etapas que cumplen la condición anterior

SELECT COUNT(*) FROM (
  SELECT numetapa FROM puerto
  GROUP BY numetapa HAVING AVG(altura)>1500
  )c1;

-- (17) Listar el dorsal del ciclista con el número de veces que ha llevado algún maillot

SELECT dorsal, COUNT(*) FROM lleva
GROUP BY dorsal;

-- (18) Listar el dorsal del ciclista con el código de maillot y cuántas veces ese ciclista ha llevado ese maillot

SELECT dorsal, código, COUNT(*) FROM lleva
  GROUP BY dorsal, código;

-- (19) Listar el dorsal, el código de etapa, el ciclista y el número de maillots que ese ciclista ha llevado en cada etapa

SELECT dorsal, numetapa, COUNT(*) FROM lleva
  GROUP BY dorsal, numetapa;



