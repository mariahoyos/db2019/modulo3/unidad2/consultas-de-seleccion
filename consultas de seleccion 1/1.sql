﻿USE ciclistas;
-- (1) listar las edades de los ciclistas (sin repetidos)

SELECT DISTINCT edad FROM ciclista;

-- (2) listar las edades de los ciclistas de Artiach

SELECT DISTINCT edad FROM ciclista
  WHERE nomequipo = 'Artiach';

-- (3) listar las edades de los ciclistas de Artiach o de Amore Vita

SELECT DISTINCT edad FROM ciclista
  WHERE nomequipo = 'Artiach' OR nomequipo = 'Amore Vita';

-- (4) listar los dorsales de los ciclistas cuya edad sea menor que 25 o mayor que 30

SELECT dorsal FROM ciclista
  WHERE edad<24 OR edad>30;

-- (5) listar los dorsales de los ciclistas cuya edad esté entre 28 y 32 y además que sólo sean de Banesto

SELECT dorsal FROM ciclista
  WHERE (edad BETWEEN 28 AND 32) AND (nomequipo = 'Banesto');

-- (6) Indícame el nombre de los ciclistas que el número de caracteres del nombre sea mayor que 8

SELECT DISTINCT nombre FROM  ciclista
  WHERE CHAR_LENGTH(nombre)>8;

-- (7) lístame el nombre y el dorsal de todos los ciclistas mostrando un campo nuevo denominado nombre mayúsculas que debe mostrar el nombre en mayúsculas

SELECT DISTINCT nombre, UPPER(nombre) nombre_mayúsculas, dorsal FROM ciclista;

-- (8) Listar todos los ciclistas que han ganado el maillot MGE (amarillo) en alguna etapa

  SELECT DISTINCT dorsal FROM lleva
    WHERE código='MGE';

-- (9) Listar el nombre de los puertos cuya altura sea mayor de 1500 

SELECT nompuerto FROM puerto
  WHERE altura>1500;

-- (10) Listar el dorsal de los ciclistas que hayan ganado algun puerto cuya pendiente sea mayor que 8 o cuya altura esté entre 1800 y 3000

SELECT DISTINCT dorsal FROM puerto
  WHERE pendiente>8 OR altura BETWEEN 1800 AND 3000;

-- (11) Listar el dorsal de los ciclistas que hayan ganado algún puerto cuya pendiente sea mayor que 8 y cuya altura esté entre 1800 y 3000

SELECT DISTINCT dorsal FROM puerto
  WHERE pendiente>8 AND altura BETWEEN 1800 AND 3000;